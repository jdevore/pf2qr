import {critCheck} from "./crit-test.js";
import {showResults} from "./show-results.js";
import {generateChatMessage} from "./generate-chat-message.js";

export function compareAttacks(message) {
    let compiledMessage = `<h3 style='border-bottom: 3px solid black'>Attacks:</h3>`;
    let maxSuccessStep = 0
    game.user.targets.forEach(token => {

        //Success step meaning:
        // 3 = Critical
        // 2 = success
        // 1 = failure
        // 0 = critical failure
        let successStep = -1;
        const armorClass = token.actor.data.data.attributes.ac.value;

        //getting the base level of success from the roll:
        if (message.roll.total >= armorClass) {
            successStep = 2;
        } else {
            successStep = 1;
        }

        //Augmenting the success by criticals and natural 20s/1s:
        successStep += critCheck(message.roll, armorClass);

        //Ensuring the successStep doesn't somehow break the system catastrophically?
        successStep = Math.clamp(successStep, 0, 3);

        maxSuccessStep = Math.max(maxSuccessStep, successStep);

        const successBy = message.roll.total - armorClass;
        message.roll.dice[0].options.critical = successStep >= 3 ? 1 : null
        message.roll.dice[0].options.fumble = successStep === 0 ? 999 : null;
        compiledMessage += generateChatMessage(token, successStep, successBy, true);
        // Get the sounds
    const criticalSuccessFailureTracks = game.settings.get("maestro","criticalSuccessFailureTracks");
    const criticalSuccessPlaylist = criticalSuccessFailureTracks.criticalSuccessPlaylist;
    const criticalSuccessSound = criticalSuccessFailureTracks.criticalSuccessSound;
    const criticalFailurePlaylist = criticalSuccessFailureTracks.criticalFailurePlaylist;
    const criticalFailureSound = criticalSuccessFailureTracks.criticalFailureSound;
       
    // Play relevant sound for successes and failures
    if (successStep === 3) {
        window.Playback.playTrack('random-track', criticalSuccessPlaylist);
    } else if (successStep === 0) {
        window.Playback.playTrack('random-track', criticalFailurePlaylist)
    }        
    });

    //Determining permissions, and whether to show result or not:
    if (game.user.targets.size > 0) {
        const chatData = {
            user: game.user.data._id,
            content: compiledMessage
        }
        showResults(chatData);
    }

    if (message.data.flavor.includes('Spell Attack')) return;

    if (maxSuccessStep >= 2 && game.settings.get("pf2qr", "RollDamageOnHit") && (game.user.isGM || !game.settings.get("pf2qr", "OnlyAutoRollGmDamage"))) {
        
        const token = canvas.tokens.placeables.find(a => a.data.actorId === message.data.speaker.actor);

        if (!token){
            ui.notifications.error("Attacker not found, token whose strike this is is most likely deleted.  Re-add them to the scene to resolve.");
            return;
        }

    	const matches = message.data.flavor.match(/Strike: (.+?)<|(.+) - Attack Roll/);

    	if (!matches || !(matches[1] || matches[2])){
            ui.notifications.error("Unable to determine name of strike.  Please submit an issue to https://gitlab.com/mcarthur.alford/pf2qr/-/issues");
            return;
    	}

        const strikeName = matches[1] || matches[2];
        
        const actions = token.document._actor.data.data.actions;
		const strike = actions.find(x => x.name === strikeName && x.type === 'strike');
		maxSuccessStep === 2 ? strike.damage({}) : strike.critical({});
    }
}
